//
//  Quiz.swift
//  ToddlerApp
//
//  Created by Woosley,Terry J on 4/4/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

class Quiz: Codable{
    
    var response_code:Int = 0
    var results:[Question] = []
    
}
