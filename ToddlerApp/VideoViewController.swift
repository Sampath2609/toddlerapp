//
//  VideoViewController.swift
//  ToddlerApp
//
//  Created by Student on 3/10/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import AVKit

class VideoViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    
    
    @IBOutlet weak var videoPickerView: UIPickerView!
    
    var  pickerData = ["Maths",
                       "Physics",
                       "Computer Science",
                       "Biology"]
    var input:String = " "
    
    @IBAction func readyToWatchBTN(_ sender: Any) {
        //Segue from here to other views depending upon what subject the user selects
        switch input {
        case "Maths":
            if let path = Bundle.main.path(forResource: "video", ofType: "mp4")
            {
                let video = AVPlayer(url: URL(fileURLWithPath: path))
                let videoPlayer = AVPlayerViewController()
                videoPlayer.player = video
                present(videoPlayer, animated: true)
                {
                    video.play()
                }
        }
        case "Physics":
            if let path = Bundle.main.path(forResource: "video2", ofType: "mp4")
            {
                let video = AVPlayer(url: URL(fileURLWithPath: path))
                let videoPlayer = AVPlayerViewController()
                videoPlayer.player = video
                present(videoPlayer, animated: true)
                {
                    video.play()
                }
            }
        case "Computer Science":
            if let path = Bundle.main.path(forResource: "computer", ofType: "mp4")
            {
                let video = AVPlayer(url: URL(fileURLWithPath: path))
                let videoPlayer = AVPlayerViewController()
                videoPlayer.player = video
                present(videoPlayer, animated: true)
                {
                    video.play()
                }
            }
        case "Biology":
            if let path = Bundle.main.path(forResource: "bio", ofType: "mp4")
            {
                let video = AVPlayer(url: URL(fileURLWithPath: path))
                let videoPlayer = AVPlayerViewController()
                videoPlayer.player = video
                present(videoPlayer, animated: true)
                {
                    video.play()
                }
            }
            
            
        default:
            return
        }
    }
    
    //UIPickerView conformity starts
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        input = pickerData[row]
        myImage()
    }
    //UIPickerView conformity ends
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.videoPickerView.delegate = self
        self.videoPickerView.dataSource = self
    }
    func myImage()  {
        switch input {
        case "Biology":
            subjectImage.image = #imageLiteral(resourceName: "dna.jpg")
        case "Computer Science":
            subjectImage.image = #imageLiteral(resourceName: "computer.jpg")
        case "Physics":
            subjectImage.image = #imageLiteral(resourceName: "hqdefault.jpg")
        case "Maths":
            subjectImage.image = #imageLiteral(resourceName: "maxresdefault.jpg")
        default:
            return
        }
    }
  

    @IBOutlet weak var subjectImage: UIImageView!
    
}
