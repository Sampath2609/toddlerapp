//
//  NumberOfQuestionsViewController.swift
//  ToddlerApp
//
//  Created by Terry Woosley on 4/20/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class NumberOfQuestionsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var numberPicker: UIPickerView!
    
    let number:[String] = ["1","2","3", "4", "5", "6", "7", "8", "9", "10"]
    var numberInput:String = ""
    var subject:String = ""
    var level:String = ""
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return number.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return number[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        numberInput = number[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.numberPicker.dataSource = self
        self.numberPicker.delegate = self
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toQuestions"){
            let questionsView = segue.destination as! QuestionsViewController
            questionsView.subject = subject
            questionsView.level = level
            questionsView.numberOfQuestions = numberInput
        }
        
    }

}
