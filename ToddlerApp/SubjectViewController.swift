//
//  SubjectViewController.swift
//  ToddlerApp
//
//  Created by Terry Woosley on 4/20/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class SubjectViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var subjectPicker: UIPickerView!
    
    let subject:[String] = ["Math", "Computer Science", "Biology"]
    var subjectInput:String = ""
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subject.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subject[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        subjectInput = subject[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.subjectPicker.dataSource = self
        self.subjectPicker.delegate = self
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toLevel"){
            let levelView = segue.destination as! LevelViewController
            switch subjectInput{
            case "Math":
                levelView.subject = "19"
            case "Computer Science":
                levelView.subject = "18"
            case "Biology":
                levelView.subject = "27"
            default:
                levelView.subject = "18"
            }
        }
        
    }

}
