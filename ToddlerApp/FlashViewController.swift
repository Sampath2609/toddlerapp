//
//  FlashViewController.swift
//  ToddlerApp
//
//  Created by Student on 4/4/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class FlashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func lionFlip(_ sender: UIButton) {
        flipCard(withEmoji:"🦁",on:sender)
    }
    
    
     @IBAction func crabFlip(_ sender: UIButton) {
         flipCard(withEmoji:"🦀",on:sender)
     }
    
    @IBAction func fishFlip(_ sender: UIButton) {
        flipCard(withEmoji: "🐠", on:sender)
    }
    func flipCard(withEmoji emoji:String,on button:UIButton){
        if button.currentTitle == "🦁"{
            button.setTitle("lion", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
       else  if button.currentTitle == "🦀"{
            button.setTitle("crab", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
       else  if button.currentTitle == "🐠"{
            button.setTitle("fish", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
        else if button.currentTitle == "lion" {
            button.setTitle("🦁", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "crab" {
            button.setTitle("🦀", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "fish" {
            button.setTitle("🐠", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        
        
    }
}
