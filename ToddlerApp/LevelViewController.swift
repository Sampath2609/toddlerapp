//
//  LevelViewController.swift
//  ToddlerApp
//
//  Created by Student on 3/11/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class LevelViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    let level:[String] = ["Easy","Medium","Hard"]
    var levelInput:String = ""
    var subject:String = ""
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return level.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return level[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        levelInput = level[row]
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.levelPicker.dataSource = self
        self.levelPicker.delegate = self
    }
   
    @IBOutlet weak var levelPicker: UIPickerView!
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "toNumber"){
            let numberView = segue.destination as! NumberOfQuestionsViewController
            numberView.subject = subject
            numberView.level = levelInput.lowercased()
        }
     
    }

}
