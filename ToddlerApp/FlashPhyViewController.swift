//
//  FlashPhyViewController.swift
//  ToddlerApp
//
//  Created by Student on 4/23/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class FlashPhyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func f1(_ sender: UIButton) {
           flipCard(withEmoji:"weight=?",on:sender)
    }
    
    @IBAction func f2(_ sender: UIButton) {
         flipCard(withEmoji:"Acceleration=?",on:sender)
    }
    
    @IBAction func f3(_ sender: UIButton) {
         flipCard(withEmoji:"Avg Speed=?",on:sender)
    }
    
    func flipCard(withEmoji emoji:String,on button:UIButton){
        if button.currentTitle == "weight=?"{
            button.setTitle("mass * velocity", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
        else  if button.currentTitle == "Acceleration=?"{
            button.setTitle("Change in velocity / time", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
        else  if button.currentTitle == "Avg Speed=?"{
            button.setTitle("Distance / time", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
        else if button.currentTitle == "mass * velocity" {
            button.setTitle("weight=?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "Change in velocity / time" {
            button.setTitle("Acceleration=?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "Distance / time" {
            button.setTitle("Avg Speed=?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        
        
        
        
        
        
    }

}
