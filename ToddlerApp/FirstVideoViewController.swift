//
//  FirstVideoViewController.swift
//  ToddlerApp
//
//  Created by Student on 4/22/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit
import AVKit

class FirstVideoViewController: UIViewController {
    
    
    @IBAction func buttonAction(_ sender: Any)
    {
        if let path = Bundle.main.path(forResource: "video", ofType: "mp4")
        {
            let video = AVPlayer(url: URL(fileURLWithPath: path))
            let videoPlayer = AVPlayerViewController()
            videoPlayer.player = video
            
            
            
            present(videoPlayer, animated: true)
            {
                video.play()
            }
        }
        
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
