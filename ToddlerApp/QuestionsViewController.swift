//
//  QuestionsViewController.swift
//  ToddlerApp
//
//  Created by Student on 4/3/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var answersTableView: UITableView!
    @IBOutlet weak var questionTV: UITextView!
    @IBOutlet weak var correctAnswersLBL: UILabel!
    
    
    var quiz:Quiz?
    var currentQuestion:Int = 0
    var correctAnswers:Int = 0
    var selectedAnswer:String?
    var subject:String = ""
    var level:String = ""
    var numberOfQuestions:String = ""
    
    @IBAction func nextQuestionBTN(_ sender: Any) {
        if let index = answersTableView.indexPathForSelectedRow, let q = quiz{
            if index.row == q.results[currentQuestion].incorrect_answers.count {
                correctAnswers += 1
            }
        }
        if(currentQuestion < quiz!.results.count - 1){
            currentQuestion += 1
            answersTableView.reloadData()
        }
        else{
            correctAnswersLBL.text! = String(correctAnswers)
            print(correctAnswers)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        QuizFetcher.shared.fetchQuestions(category: subject, amount: numberOfQuestions, difficulty: level)
        
        NotificationCenter.default.addObserver(self, selector:#selector(loadQuiz(notification:)),name: NSNotification.Name(rawValue:"NewQuiz"), object:nil)
    }
    
    @objc
    func loadQuiz(notification: Notification) {
        quiz = QuizFetcher.shared.quiz
        print(quiz?.results.count ?? "Its nill")
        DispatchQueue.main.async {
            self.answersTableView.reloadData()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        answersTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if(quiz != nil){
            self.questionTV.text = quiz?.results[currentQuestion].question
        }
        return 1
    }
    
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionCell")
        print(quiz?.results[currentQuestion])
        if(quiz!.results[currentQuestion].incorrect_answers.count==indexPath.row){
            cell?.textLabel?.text = quiz!.results[currentQuestion].correct_answer
        } else {
            cell?.textLabel?.text = quiz!.results[currentQuestion].incorrect_answers[indexPath.row]
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(quiz==nil){
            return 0
        }
        return quiz!.results[currentQuestion].incorrect_answers.count + 1
    }
    
    
}
