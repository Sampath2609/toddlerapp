//
//  Question.swift
//  ToddlerApp
//
//  Created by Woosley,Terry J on 4/4/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

struct Question: Codable{
    
    var category:String
    var type:String
    var difficulty:String
    var question:String
    var correct_answer:String
    var incorrect_answers:[String]
    
}
