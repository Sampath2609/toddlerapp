//
//  FlashMathViewController.swift
//  ToddlerApp
//
//  Created by Student on 4/23/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class FlashMathViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func formula1(_ sender: UIButton) {
        flipCard(withEmoji:"(a+b)^2?",on:sender)
    }
    
    @IBAction func formula2(_ sender: UIButton) {
        flipCard(withEmoji:"(a-b)^2?",on:sender)

    }
    
    
    @IBAction func formula3(_ sender: UIButton) {
        flipCard(withEmoji:"(a+b)^(a-b)?",on:sender)

    }
    
    
    func flipCard(withEmoji emoji:String,on button:UIButton){
        if button.currentTitle == "(a+b)^2?"{
            button.setTitle("a^2+2ab+b^2", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        }
        else  if button.currentTitle == "(a-b)^2?"{
            button.setTitle("a^2-2ab+b^2", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
        else  if button.currentTitle == "(a+b)^(a-b)?"{
            button.setTitle("a^2-b^2", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            
        }
        else if button.currentTitle == "a^2+2ab+b^2" {
            button.setTitle("(a+b)^2?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "a^2-2ab+b^2"{
            button.setTitle("(a-b)^2?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        else if button.currentTitle == "a^2-b^2" {
            button.setTitle("(a+b)^(a-b)?", for: UIControl.State.normal)
            button.backgroundColor=#colorLiteral(red: 1, green: 0.9426102406, blue: 0.1029559747, alpha: 1)
        }
        
        
    }
}
