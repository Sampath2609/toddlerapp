//
//  QuizFetcher.swift
//  ToddlerApp
//
//  Created by Terry Woosley on 4/20/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation

class QuizFetcher{
    
    static var shared = QuizFetcher()
    
    private init(){}
    
    var quiz:Quiz?
    
    func fetchQuestions(category:String, amount:String, difficulty:String) {
        
        let urlSession = URLSession.shared
        let url = URL(string: "https://opentdb.com/api.php?amount=\(amount)&category=\(category)&difficulty=\(difficulty)&type=multiple")
        urlSession.dataTask(with:url!, completionHandler:returnQuiz).resume()
    }
    
    func returnQuiz(data:Data?, urlResponse:URLResponse?, error:Error?){
        
        do{
            let decoder = JSONDecoder()
            quiz = try decoder.decode(Quiz.self, from:data!)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"NewQuiz"), object: nil)
        }
        catch{
            print(error)
        }
    }
    
}
